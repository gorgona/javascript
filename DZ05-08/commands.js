/**
 * Created by Админ on 06.08.2016.
 */
/**
 * Created by Elena on 05.08.2016.
 * 2. Еще раз переделайте вот эти вот задания, но 5 пункте реализовать для всех указанных методов массива (map, reduce, forEach):

 https://github.com/bi0morph/teaching-materials/tree/gh-pages/functional-lite-js/exercises/ex4



 Написать две функции, каждая возвращает разные значения, когда вызываются.


 Написать add(..) функцию, которая принимает два числа и складывает их, и возвращает результат. Вызовите add(..) с результатами ваших двух функций из пункта (1) и выведте результат в консоль.

 Написать add2(..) которая принимает две функции в отличии от двух цифр, и вызывает эти две функции, и передает их результат в add(..), точно так же как вы делали в пункте(2) выше.

 Замените ваши две функции из пункта (1) одной функцией, которая принимает значение и возвращает функцию, которая при вызове будет возвращать значение, которое было принято в начале.

 Написать addn(..), которая принимает массив функций, и используя только add2(..) складывать их вместе. Реализовать это через цикл. Реализовать без цикла (рекурси). Реализовать встроенными методами(map, reduce).

 Создайте массив с четными, нечетными и повторяющимися числами. Уберите дубликаты встроенными методами.

 Отфильтруйте ваш массив, так что бы в нем были только четные числа.
 */

/* Написать две функции, каждая возвращает разные значения, когда вызываются. */
console.log('begin');
function number1(){
    return Math.random()*10;
}
function number2(){
    return Math.random()*10;
}
/* Написать add(..) функцию, которая принимает два числа и складывает их, и возвращает результат.
 Вызовите add(..) с результатами ваших двух функций из пункта (1) и выведте результат в консоль.
 */


function add(arg1,arg2){
    return arg1+arg2;
}
function addButton(){
    console.log(add(n1=number1(),n2=number2())+' = '+n1+' + '+n2);


}

/* Написать add2(..) которая принимает две функции в отличии от двух цифр, и вызывает эти две функции,
 и передает их результат в add(..), точно так же как вы делали в пункте(2) выше.
 */

function add2(func1,func2){
    var sum = add(n1=func1(),n2=func2());
    console.log(sum +' = '+n1+' + '+n2);
    return sum;
}

function add2Button(){
    console.log(add(n1=number1(),n2=number2())+' = '+n1+' + '+n2);


}

/*
 Замените ваши две функции из пункта (1) одной функцией, которая принимает значение и возвращает функцию,
 которая при вызове будет возвращать значение, которое было принято в начале.*/

function returnFunction(argument) {
    var func = function () {
        return (argument);
    };
    return func;
}

function returnFunctionButton() {
    b = returnFunction(54);
    b();
    console.log('функция как аргумент');

    console.log(add2(returnFunction(number1()), returnFunction(number1())));

}
//Написать addn(..), которая принимает массив функций, и используя только add2(..) складывать их вместе.
//   Реализовать это через цикл. Реализовать без цикла (рекурси). Реализовать встроенными методами(map, reduce).

var _argument=[];

_argument.push(number1);
_argument.push(number2);
_argument.push(number1);
_argument.push(number1);
_argument.push(number1);
_argument.push(number1);

function addn1(_argument){
    var summa=0;
    for (i = 0; i < _argument.length; i++ ){
        num1 =_argument[i]();
        console.log(num1);
        summa+=num1;
    }
    return summa;
}

function addn1Button() {
    var summa = addn1(_argument);
    console.log('функция addn1');
    console.log(summa);
}
/* Реализовать без цикла (рекурси)*/

var summa = 0;

function addn2(_argument){
    console.log(_argument);
    if (_argument.length===0){
        return result;
    }
    else {
        result = function(){
            summa=add2(_argument[0],addn2(_argument.slice(1)));
            return summa;
        }
    }

}

function addn2Button() {
    var result = [];
    result = addn2(_argument);
    console.log('функция addn2');
    console.log(result);
}

function addn3(_argument){
    var summa=0;

    _argument.map(function (item){
        var _v = item();
        console.log(_v);
        summa+=_v;
    }) ;
    return summa;
}

function addn3Button() {
    console.log('функция addn3');
    var result = addn3(_argument)
    console.log('Итого '+result);
}

function addn4(_argument){
    var cur;
    var result = _argument.reduce(function(sum, current) {
        cur = current();
        console.log(cur);
        return sum + cur;
    }, 0);
    return result;
}
function addn4Button() {
    console.log('функция add4');
    var result = addn4(_argument);
    console.log('Итого '+result);

}


//Создайте массив с четными, нечетными и повторяющимися числами. Уберите дубликаты встроенными методами.
// Отфильтруйте ваш массив, так что бы в нем были только четные числа. */

function createArr() {
    var myArr = [];
    var a;
    for (i = 0; i < 50; i++) {
        a=Math.round(number1());
        console.log('a= '+a);          myArr.push(a);
    }
    return myArr;
}


var myArr = createArr();

function createArrButton() {
    console.log('создаем массив '+myArr);

}

function newArrDistinct(myArr){
    var newArr = [];
    for ( i = 0; i < myArr.length; i++ ){

        if(newArr.indexOf(myArr[i])==-1){
            newArr.push(myArr[i]);
        }
    }
    return newArr;
}

function newArrDistinctButton() {
    console.log('массив без повторений ');
    console.log(newArrDistinct(myArr));

}


function onlyChetArrButton() {
    var onlyChetArr = myArr.filter(function(number) {
        if(number%2 === 0){
            return number;
        }
    });
    console.log('только четные ' + onlyChetArr);

}

function onlyNeChetArrButton() {
    var onlyNeChetArr = myArr.filter(function (number) {
        if (number % 2 != 0) {
            return number;
        }
    });

    console.log('только нечетные ' + onlyNeChetArr);

}
// Еще раз переделайте вот эти вот задания, но 5 пункте реализовать для всех указанных методов массива (map, reduce, forEach):
//Создайте массив с четными, нечетными и повторяющимися числами. Уберите дубликаты встроенными методами.
// Отфильтруйте ваш массив, так что бы в нем были только четные числа. */
/*
 ForEach
 */
function newArrDistinctForEach(myArr){
    var newArr = [];
    myArr.forEach(function(item, i, myArr){
        if(newArr.indexOf(item) == -1){
            newArr.push(item);
        }
    })
    return newArr;
}

function newArrDistinctForEachButton() {
    console.log('массив без повторений forEach');
    console.log(newArrDistinctForEach(myArr));

}
/*
 map
 */

function newArrDistinctMap(myArr){
    var newArr=[];
    myArr.map(function(item){
        if(newArr.indexOf(item) == -1){
            newArr.push(item);
        }
    });
    return newArr;
}
function newArrDistinctMapButton() {
    console.log('массив без повторений map ');
    result = newArrDistinctMap(myArr);
    console.log(result);
}
/*
 reduce
 */

function newArrDistinctReduce(myArr){

    newArr = myArr.reduce(function(newArr,number){
        if(newArr.indexOf(number) == -1){
            newArr.push(number);
        }
        return newArr;
    },[]);

    return newArr;
}

function newArrDistinctReduceButton() {
    console.log('массив без повторений reduce');
    result = newArrDistinctReduce(myArr);
    console.log(result);
}

/* ForEach*/
function onlyChetArrForEach(_agument) {
    var onlyChetArrForEach = []
    myArr.forEach(function (number) {
        if (number % 2 === 0) {
            onlyChetArrForEach.push(number);
        }
    })
    return onlyChetArrForEach;
}

function onlyChetArrForEachButton() {
    result = onlyChetArrForEach(_argument);
    console.log('только нчетные forEach ' +result);
}

/* map*/

function onlyChetArrMap(_argument) {
    var onlyChetArrMap = [];
    onlyChetArrMap = myArr.map(function (number) {
        if (number % 2 === 0) {
            return number;
        }
    })
    return onlyChetArrMap;
}
function onlyChetArrMapButton() {
    result =onlyChetArrMap(_argument);
    console.log('только четные map ' + result);
};

/* reduce  */
function onlyChetArrReduce() {
    var onlyChetArrReduce = [];

    onlyChetArrReduce = myArr.reduce(function (newArr, number) {
        if (number % 2 === 0) {
            newArr.push(number);
        }
        return newArr;
    }, []);
    return onlyChetArrReduce;
}

function onlyChetArrReduceButton() {
    result =  onlyChetArrReduce();
    console.log('только ечетные reduce' + result);
}

function onlyNeChetArrForEach(_agument) {
    var onlyNeChetArrForEach = []
    myArr.forEach(function (number) {
        if (number % 2 !== 0) {
            onlyNeChetArrForEach.push(number);
        }
    })
    return onlyNeChetArrForEach;
}

function onlyNeChetArrForEachButton() {
    result = onlyNeChetArrForEach(_argument);
    console.log('только нечетные forEach ' +result);
}

/* map*/

function onlyNeChetArrMap(_argument) {
    var onlyNeChetArrMap = [];
    onlyNeChetArrMap = myArr.map(function (number) {
        if (number % 2 !== 0) {
            return number;
        }
    })
    return onlyNeChetArrMap;
}
function onlyNeChetArrMapButton() {
    result =onlyNeChetArrMap(_argument);
    console.log('только нечетные map ' + result);
};

/* reduce  */
function onlyNeChetArrReduce() {
    var onlyNeChetArrReduce = [];

    onlyNeChetArrReduce = myArr.reduce(function (newArr, number) {
        if (number % 2 !== 0) {
            newArr.push(number);
        }
        return newArr;
    }, []);
    return onlyNeChetArrReduce;
}

function onlyNeChetArrReduceButton() {
    result =  onlyNeChetArrReduce();
    console.log('только нечетные reduce' + result);
}




