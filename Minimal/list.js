/**
 * Created by ����� on 11.08.2016.
 */
var list = {
    init: function(id, onChange){
        var source = document.querySelector(id).innerHTML;
         this.template = Handlebars.compile(source);

        this.getData(onChange);
        this.user = [];
        // ������������ handlebar
    },
    render: function(id){
        var context = {
            user: list.users

        };
        var html = this.template(context);

        return html;
    },
    getData: function(callback){
        this.user =[];
        var promise = fetch('https://jsonplaceholder.typicode.com/users');
        promise.then(function(respon){
           return respon.json();
        }).then(function(result){
            list.users = result;
            callback();
        }).catch(function(error){
            console.log(error);
        })
    }
}
