window.isMobile = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

var Analysis = function() {
    var board; /* object ChessBoard */
    var game; /* object Chess */
    var gameHistory;
    var currentPly;
    var aGameHistory = [];
    var e = {
        board:          $('#board'),
        startBtn:       $('.game-controls .btn-start'),
        endBtn:         $('.game-controls .btn-end'),
        previousBtn:    $('.game-controls .btn-previous'),
        nextBtn:        $('.game-controls .btn-next'),
        playBtn:        $('.game-controls .btn-play'),
        flipBtn:        $('.game-controls .btn-flip'),
        fen:            $('#fen'),
        evalBtn:        $('#evalControls button'),
        evalOn:         $('#evalControls .eval-on'),
        evalOff:        $('#evalControls .eval-off'),
        evalScale:      $('#evaluation'),
        evalBlackScale: $('#evaluation .black'),
        moves:          $('#game .panel-body'),
        commentTxt:     $('#addComment input'),
        commentBtn:     $('#addComment button')
    };
    var depth = 30;
    var autoPlay = false;
    var playDelay = 1000;
    var squareClass = 'square-55d63';
    var evaluation = false;
    var engine = null;
    var scaleTimeout = null;
    var promotion = new Promotion();

    return {
        init : init,
        setPgn: setPgn,
        getAnalysis: getAnalysis,
        getCurrentPly: getCurrentPly
    };

    ////////////

    function init(options) {
        playDelay = options.playDelay || playDelay;
        depth = options.depth || depth;

        options.onChange    = highlightMove;
        options.position    = 'start';
        options.draggable   = true;
        options.onDragStart = onDragStart;
        options.onDrop      = onDrop;
        options.onSnapEnd   = onSnapEnd;

        board = new ChessBoard('board', options);
        $(window).resize(board.resize);

        initControls();
    }

    function setPgn(pgn, ply) {
        game = new Chess();
        game.load_pgn(pgn);

        $("#game .result").text(game.header().Result);

        gameHistory = game.history({verbose: true});

        console.log(ply);
        ply = ply || -1;
        toMove(ply);
        highlightControls();

        if(!window.isMobile()) {
            toggleEvaluation();
        }
    }

    function initControls() {
        e.startBtn.on('click', toStart);
        e.previousBtn.on('click', toPrevious);
        e.nextBtn.on('click', toNext);
        e.endBtn.on('click', toEnd);
        e.playBtn.on('click', toggleAutoplay);
        e.flipBtn.on('click', board.flip);
        e.evalBtn.on('click', toggleEvaluation);
        e.commentBtn.on('click', addComment);
        e.commentTxt.on('keyup', function(e) {
            if(e.keyCode == 13) addComment();
        });

        // move
        $('#game .move a').on('click', function () {
            var ply = $(this).data('ply');
            toMove(ply);

            aGameHistory = [];

            return false;
        });

        // analysis move
        $('#game .alt .aply').on('click', toAltMove);
        // analysis remove
        $('#game .close').on('click', removeComment);

        $(document).keydown(function(ev) {
            switch(ev.which) {
                case 37: // left
                    toPrevious();
                    break;
                case 39: // right
                    toNext();
                    break;
                default: return;
            }
            ev.preventDefault();
        });
    }

    // -----------------------------------------------------------------------------------------------------------------

    function toAltMove() {
        var $ply = $(this);

        var startPly = $ply.closest('tr').data('ply');
        toMove(startPly - 1, true); // last ply before analysis

        // analysis moves
        var altPlys = [$ply.text()];
        $ply.prevAll('.aply').each(function(i, el) {
            altPlys.push($(el).text());
        });
        altPlys.reverse();

        var lastMove;
        for(var i in altPlys) {
            lastMove = game.move(altPlys[i]);
            aGameHistory.push(lastMove);
        }

        board.position(game.fen());

        highlightMove(null, null, lastMove);
        $('#game .alt a').removeClass('highlight');
        $ply.addClass('highlight');

        return false;
    }

    function onDragStart(from, piece, position, orientation) {
        if (game.game_over() === true ||
            (game.turn() === 'w' && piece.search(/^b/) !== -1) ||
            (game.turn() === 'b' && piece.search(/^w/) !== -1)) {
            return false;
        }
    }

    function onDrop(from, to, piece, pos1, pos2, color) {
        var tmpGame = new Chess(game.fen());
        var tmpMove = tmpGame.move({
            from: from,
            to: to,
            promotion: 'q'
        });

        if(tmpMove) {
            if (!promotion.isNeeded(from, to, piece, color)) {
                return doMove(from, to);
            } else {
                promotion.ask().then(function (p) {
                    if(p) {
                        doMove(from, to, p);
                    } else {
                        board.position(game.fen());
                        if(aGameHistory.length) {
                            highlightMove(null, null, aGameHistory[aGameHistory.length - 1]);
                        }
                    }
                });
            }
        } else {
            return 'snapback';
        }
    }

    function doMove(from, to, promo) {
        var move = game.move({
            from: from,
            to: to,
            promotion: promo
        });

        // illegal move
        if (move === null) {
            return 'snapback';
        } else {
            aGameHistory.push(move);

            if(promo) {
                board.position(game.fen());
            }
            highlightMove(null, null, move);

            $('#game .alt a').removeClass('highlight');

            // comment
            var aPgn = '';
            for (var i = 0; i <= aGameHistory.length - 1; i++) {
                if(aGameHistory[i].color == 'w' || i == 0) {
                    var index = Math.ceil((currentPly + 1 + i + 1) / 2);
                    if(aGameHistory[i].color == 'w') {
                        aPgn += '<span>' + index + '. </span>';
                    } else if(i == 0) {
                        aPgn += '<span>' + index + '... </span>';
                    }
                }
                aPgn += '<a href="#" class="aply ' + (i < aGameHistory.length - 1 ? '' : 'highlight') + '">' + aGameHistory[i].san + '</a> ';
            }

            var comment = $('<tr class="alt" data-ply="' + (currentPly + 1) + '"><td colspan="5">' + aPgn + '<a class="close">&times;</a></td></tr>');
            comment.find('.aply').on('click', toAltMove);
            comment.find('.close').on('click', removeComment);

            $tr = $('#game .move-' + Math.ceil((currentPly + 1 + 1) / 2));
            $nextTxt = $tr.next('.text');
            if(aGameHistory.length > 1) {
                if(!$nextTxt.length) {
                    $tr.next('.alt').remove();
                } else {
                    $nextTxt.next('.alt').remove();
                }
            }

            if(!$nextTxt.length) {
                $tr.after(comment);
            } else {
                $nextTxt.after(comment);
            }
        }
    }

    function onSnapEnd(from, to, piece) {
        highlightMove(null, null, {
            from: from,
            to: to
        });
    }

    function removeComment() {
        $(this).parents('tr').remove();
    }

    function addComment() {
        var txt = e.commentTxt.val();
        if(!txt) return;

        var comment = $('<tr class="text"><td colspan="5">' + txt + '<a class="close">&times;</a></td></tr>');
        comment.find('.close').on('click', removeComment);

        if(!aGameHistory.length) {
            var $tr = $('#game .move-' + Math.ceil((currentPly + 1) / 2));
        } else {
            var $tr = $('#game .move-' + Math.ceil((currentPly + 1 + 1) / 2));
        }
        $tr.next('.text').remove();
        $tr.after(comment);
    }

    function showComment() {
        var txt = $('#game .move-' + Math.ceil((currentPly + 1) / 2)).next('.text').text();
        if(!txt) {
            e.commentTxt.val('');
            return;
        }
        e.commentTxt.val(txt.slice(0, -1));
    }

    // -----------------------------------------------------------------------------------------------------------------

    function toMove(ply, noBoard) {
        if(!isNaN(ply)) {
            if (ply > gameHistory.length - 1) {
                ply = gameHistory.length - 1;
            }
            game.reset();
            for (var i = 0; i <= ply; i++) {
                game.move(gameHistory[i].san);
            }
            currentPly = i - 1;

            if(!noBoard) {
                board.position(game.fen());
            }

            aGameHistory = [];
            $('#game .alt a').removeClass('highlight');
        }
    }

    function toStart() {
        game.reset();
        currentPly = -1;
        board.position(game.fen());

        aGameHistory = [];
        $('#game .alt a').removeClass('highlight');
    }

    function toEnd() {
        while(currentPly < gameHistory.length - 1) {
            currentPly++;
            game.move(gameHistory[currentPly].san);
        }
        board.position(game.fen());

        aGameHistory = [];
        $('#game .alt a').removeClass('highlight');
    }

    function toPrevious() {
        if(!aGameHistory.length) {
            if (currentPly >= 0) {
                game.undo();
                currentPly--;
                board.position(game.fen());
            }
        } else {
            toMove(currentPly);
            board.position(game.fen());
        }
    }

    function toNext() {
        $('#game .alt a').removeClass('highlight');
        for(var i = 0; i < aGameHistory.length; i++) {
            game.undo();
        }

        if (currentPly < gameHistory.length - 1) {
            aGameHistory = [];
            currentPly++;
            game.move(gameHistory[currentPly].san);
            board.position(game.fen());
        }
    }

    function toggleAutoplay() {
        var state = e.playBtn.data('state');
        if(!state || state == 'pause') {
            e.playBtn.html('<i class="fa fa-pause">');
            e.playBtn.data('state', 'play');
            autoPlay = true;

            play();
        } else {
            e.playBtn.html('<i class="fa fa-play">');
            e.playBtn.data('state', 'pause');
            autoPlay = false;
        }

        if(evaluation) {
            toggleEvaluation();
        }
    }

    function play() {
        if(autoPlay) {
            toNext();
            setTimeout(play, playDelay);
        }
    }

    function highlightMove(oldPos, newPos, move) {
        if(!move) {
            //$('#game .alt a').removeClass('highlight');

            //highlight the current move
            $("#game [class^='ply-']").removeClass('highlight');
            $('#game .ply-' + currentPly).addClass('highlight');

            // scroll moves
            e.moves.scrollTop(e.moves.scrollTop() + ($('.ply-' + currentPly).position() ?  $('.ply-' + currentPly).position().top - 250 : - e.moves.scrollTop()));

            move = gameHistory[currentPly];
        } else {
            $("#game [class^='ply-']").removeClass('highlight');
        }

        //$('#game .alt a').removeClass('highlight');

        e.board.find('.' + squareClass).removeClass('highlight');
        if(move) {
            e.board.find('.square-' + move.from).addClass('highlight');
            e.board.find('.square-' + move.to).addClass('highlight');
        }

        // write FEN
        e.fen.val(game.fen());

        highlightControls();

        // analyze position
        if(evaluation) {
            startEvaluation();
        } else {
            stopEvaluation();
        }

        showComment();
    }

    function highlightControls() {
        if(currentPly === -1) {
            e.endBtn.removeClass('disabled');
            e.nextBtn.removeClass('disabled');
            e.startBtn.addClass('disabled');
            e.previousBtn.addClass('disabled');
        } else if(currentPly === gameHistory.length -1) {
            e.startBtn.removeClass('disabled');
            e.previousBtn.removeClass('disabled');
            e.endBtn.addClass('disabled');
            e.nextBtn.addClass('disabled');
        } else {
            e.startBtn.removeClass('disabled');
            e.previousBtn.removeClass('disabled');
            e.endBtn.removeClass('disabled');
            e.nextBtn.removeClass('disabled');
        }
    }

    function toggleEvaluation() {
        evaluation = !evaluation;

        e.evalOn.html('&nbsp;');
        e.evalBlackScale.css('height', '50%');

        if(evaluation) {
            e.evalBtn.html('<span class="hidden-sm hidden-md">Analysis </span>Off');
            e.evalOff.hide();
            e.evalOn.show();

            startEvaluation();
        } else {
            e.evalBtn.html('<span class="hidden-sm hidden-md">Analysis </span>On');
            e.evalOn.hide();
            e.evalOff.show();

            stopEvaluation();
        }
    }

    function startEvaluation() {
        if(engine) {
            stopEvaluation();
        }
        e.evalScale.show();

        engine = new Worker('/stockfish/src/stockfish.js');
        engine.onmessage = parseAnalysis;

        engine.postMessage('position fen ' + game.fen());
        engine.postMessage('go depth ' + depth);
    }

    function stopEvaluation() {
        if(engine) {
            engine.postMessage('quit');
            engine.terminate();
            engine = null;

            e.evalOn.html('&nbsp;');
            e.evalBlackScale.css('height', '50%');
            e.evalScale.hide();
            e.board.find('.' + squareClass).removeClass('bestmove');
        }
    }

    function parseAnalysis(ev) {
        var line = ev.data ? ev.data : ev;
        if(line.indexOf('multipv') == -1) {
            return;
        }

        var info = new EngineMessage(line);
        var evalPercent;
        if(('' + info.evaluation).indexOf('#') == -1) {
            evalPercent = (50 - info.evaluation * 12.5).toFixed(0);
        } else {
            evalPercent = info.evaluation.replace('#', '')*1 > 0 ? 0 : 100;
        }

        if(scaleTimeout) { clearTimeout(scaleTimeout); }
        scaleTimeout = setTimeout(function() {
            e.evalBlackScale.animate({height: (evalPercent < 100 ? evalPercent + '%' : '100%')}, 200);
        }, 100);

        var evalTxt = info.evaluation > 0 ? '+' + info.evaluation : info.evaluation;
        e.evalOn.text(evalTxt);

        if(!aGameHistory.length) {
            $('#game .eval-' + currentPly).text(evalTxt);
        }

        showBestMove(info.bestMove);
    }

    function showBestMove(move) {
        move = move.match(/.{1,2}/g);

        e.board.find('.' + squareClass).removeClass('bestmove');

        e.board.find('.square-' + move[0]).addClass('bestmove');
        e.board.find('.square-' + move[1]).addClass('bestmove');
    }

    function getAnalysis() {
        var pgn = [];
        $('#game tr').each(function(i, tr) {
            var $tr = $(tr);
            if($tr.hasClass('move')) { // move
                var tds = $tr.find('td');
                pgn.push($(tds[0]).text() + '. ' + $(tds[1]).text());
                if(tds.length > 2) {
                    pgn.push($(tds[3]).text());
                }
            } else if($tr.hasClass('alt')) { // moves comment
                var comment = '(' + $tr.text().slice(0, -2) + ')';
                if($tr.data('ply') % 2) {
                    pgn.push(comment);
                } else {
                    var lastBlack = pgn.pop();
                    pgn.push(comment);
                    if(lastBlack.length < 7) {
                        var matchIndex = comment.match(/(\d+)/);
                        pgn.push(matchIndex[0] + '... ' + lastBlack);
                    } else {
                        pgn.push(lastBlack);
                    }
                }
            } else { // text comment
                var comment = '{' + $tr.text().slice(0, -1) + '}';
                var lastBlack = pgn.pop();
                pgn.push(comment);
                pgn.push(lastBlack);
            }
        });

        return pgn.join(' ') + ' ' + $('#game .result').text();
    }

    function getCurrentPly() {
        return currentPly;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function EngineMessage(line) {
        var _info = {};

        _info.message = line;
        _info.message = _info.message.trim();
        _info.message = _info.message.replace(/\s+/g,' ');

        _info.tokens  = _info.message.split(' ');

        // -----------------------------------------------------------------------------------------------------------------

        function getBestMove() {
            return getValue('pv', undefined);
        }

        function getEvaluation() {
            var normalize = game.turn() == 'b' ? -1 : 1;

            var mate = getValue('score', '') == 'mate';
            if(mate) {
                var score = getValue2('score', 0) * 1 * normalize;

                if(score > 0) {
                    return '#' + score;
                } else {
                    return '-#' + Math.abs(score);
                }
            } else {
                return ((getValue2('score', 0)/100).toFixed(2) * 1 * normalize).toFixed(2);
            }
        }

        // -----------------------------------------------------------------------------------------------------------------

        // first from key value
        function getValue(key, def) {
            var i = _info.tokens.indexOf(key);
            if(i != -1) {
                return _info.tokens[i+1];
            }
            return def;
        }

        // second from key value
        function getValue2(key, def) {
            var i = _info.tokens.indexOf(key);
            if(i != -1) {
                return _info.tokens[i+2];
            }
            return def;
        }

        // first from key to end
        function getValue3(key, def) {
            var i = _info.tokens.indexOf(key);
            if(i != -1) {
                return _info.tokens.slice(i+1).join(' ');
            }
            return def;
        }

        // -----------------------------------------------------------------------------------------------------------------

        return {
            bestMove: getBestMove(),
            evaluation: getEvaluation()
        }
    }
};

