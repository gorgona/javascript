/**
 * Created by Админ on 12.08.2016.
 */

var circles = [];
var elems = document.getElementsByTagName('div');
var squares = [];
var beginPosition = {};
document.addEventListener( "DOMContentLoaded", function(){
      circleLine();
});

var circleLine = function() {
    for (var i = 0; i < elems.length; i++) {
        if ((elems[i].className).substring(0, 6) == 'square') {
            squares.push(elems[i]);
           // setCircle = setCircle.bind(elems[i]);
            elems[i].addEventListener('contextmenu',setCircle);
            elems[i].addEventListener('dblclick',clearAll);
            //elems[i].addEventListener('click',drowLine);
            elems[i].addEventListener('mousedown',saveBeginPosition);
            elems[i].addEventListener('mouseup',drowLine);
        }
    }
}

var setCircle = function(event){
  console.log('dblClick '+ this);

    var newCircle = document.createElement('div');
    newCircle.id = 'circle';
    newCircle.style.width = this.style.width;
    newCircle.style.height = this.style.height;
    string =this.style.width;
    radius = 0.5*string.substr(0,string.length-2);
    newCircle.style.borderRadius = radius+'px';
    this.appendChild(newCircle);
    circles.push(newCircle);
    console.log(circles);
}

var clearAll = function(){
    //для кружочков
   for (var i=0; i < circles.length; i++){
       circle = document.getElementById('circle');
       circle.parentNode.removeChild(circle);
   }
    //для стрелочек

}

var anyArrow = function(){
   var newArrow = document.createElement('div');
    newArrow.id = 'rectangleArrow';
     this.appendChild(newArrow);
}

var drowLine = function(){
    newCanvas = document.createElement("canvas");
   // alert ('Рисуем линию');
    this.appendChild(newCanvas);
    if (newCanvas.getContext){
        var ctx = newCanvas.getContext('2d');
        // drawing code here
        ctx.clipToBound = false;
         ctx.beginPath();
            ctx.moveTo(10,10);
            ctx.lineTo(250,250);
        ctx.closePath();
        ctx.stroke();
    } else {
        // canvas-unsupported code here
    }
}

var savePosition = function(){
    var elem = this;
    var coord = {};
     coord = getCoords(this);
         width = this.style.width;
         height = this.style.height;
    var topPos = getCoords(this).top+deletePx(height)/2;
    var leftPos = getCoords(this).left+deletePx(width)/2;

    return {topPos, leftPos};
}

saveBeginPosition = function(){
    beginPosition = savePosition();
}

var drowArrow = function(){
 endPosition = savePosition();

    // alert ('Рисуем линию');
   newTail = document.createElement("rectangle");
    newTail.style.
    this.appendChild(newCanvas);
    if (newCanvas.getContext){
        var ctx = newCanvas.getContext('2d');
        // drawing code here
        ctx.clipToBound = false;
        ctx.beginPath();
        ctx.moveTo(10,10);
        ctx.lineTo(250,250);
        ctx.closePath();
        ctx.stroke();
    } else {
        // canvas-unsupported code here
    }
}
function deletePx(stroka){
   return (stroka.substr(0,stroka.length-2));
}

function getCoords(elem) {
    // (1)
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docEl = document.documentElement;

    // (2)
    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    // (3)
    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;

    // (4)
    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return {
        top: top,
        left: left
    };
}