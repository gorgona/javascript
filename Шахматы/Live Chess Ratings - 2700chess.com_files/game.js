var Game = function () {
    var board; /* object ChessBoard */
    var game; /* object Chess */
    var gameHistory;
    var currentPly;
    var e = {
        board:          $('#board'),
        moves:          $('#game tbody'),
        movesHolder:    $('#game .panel-body'),
        undoBtn:        $('.game-controls .btn-undo'),
        resignBtn:      $('.game-controls .btn-resign'),
        flipBtn:        $('.game-controls .btn-flip'),
        status:         $('.game-controls .status'),
        gameStatus:     $('#gameStatus'),
        result:         $('#game .result'),
        saveGameBtn:    $('#saveGame'),
        evalBtn:        $('#evalControls button'),
        evalScale:      $('#evaluation'),
        evalOn:         $('#evalControls .eval-on'),
        evalOff:        $('#evalControls .eval-off'),
        evalBlackScale: $('#evaluation .black')
    };
    var promotion   = new Promotion();
    var squareClass = 'square-55d63';
    var evaluation  = false;
    var evalEngine;
    var engine;
    var scaleTimeout = null;
    var color       = 'b';
    var skillLevel  = 1;
    var depth       = 8;
    var maxErr      = 0;
    var errProb     = 128;
    var analysisDepth = 30;

    return {
        init: init,
        getPgn: getPgn
    };

    ////////////

    function init(options) {
        // engine options
        color           = options.color || color;
        skillLevel      = options.skillLevel;

        analysisDepth   = options.depth || analysisDepth;

        /*
        if (skillLevel < 2) {
            depth = 1;
        } else if (skillLevel < 4) {
            depth = 2;
        } else if (skillLevel < 6) {
            depth = 3;
        } else if (skillLevel < 8) {
            depth = 4;
        } else {
            depth = 8;
        }
        */


        maxErr  = Math.round((skillLevel * -0.25) + 5);
        errProb = Math.round((skillLevel * 6.35) + 1);

        // board
        var cfg = {
            pieceTheme  : options.pieceTheme,
            showNotation: options.pieceTheme,
            moveSpeed   : options.pieceTheme,

            orientation : (options.orientation ? options.orientation : 'white'),
            position    : 'start',
            onChange    : highlightMove,
            draggable   : true,
            onDragStart : onDragStart,
            onDrop      : onDrop,
            onSnapEnd   : onSnapEnd
        };
        board = new ChessBoard('board', cfg);
        $(window).resize(board.resize);

        // game
        game = new Chess();
        if(options.pgn) {
            game.load_pgn(options.pgn);
        }
        gameHistory = game.history({verbose: true});
        toMove(gameHistory.length - 1);
        updateStatus();

        initControls();

        if(game.turn() == color) {
            findMove();
        }
    }

    function initControls() {
        e.flipBtn.on('click', board.flip);
        e.resignBtn.on('click', resign);
        e.undoBtn.on('click', undo);
        e.evalBtn.on('click', toggleEvaluation);

        window.onbeforeunload = function () {
            console.log(e.result.text());
            return e.result.text() ? null : 'Are you sure you want to close this page and quit the game?';
        }
    }

    function toMove(ply) {
        if(!isNaN(ply)) {
            if (ply > gameHistory.length - 1) {
                ply = gameHistory.length - 1;
            }
            game.reset();
            for (var i = 0; i <= ply; i++) {
                game.move(gameHistory[i].san);
            }
            currentPly = i - 1;

            board.position(game.fen());
        }
    }

    function onDragStart(from, piece, position, orientation) {
        if (game.in_checkmate() === true || game.in_draw() === true || piece[0] === color || e.result.text()) {
            return false;
        }
    }

    function onDrop(from, to, piece, pos1, pos2, color) {
        var tmpGame = new Chess(game.fen());
        var tmpMove = tmpGame.move({
            from: from,
            to: to,
            promotion: 'q'
        });

        if(tmpMove) {
            if (!promotion.isNeeded(from, to, piece, color)) {
                return doMove(from, to);
            } else {
                promotion.ask().then(function (p) {
                    if(p) {
                        doMove(from, to, p);
                    } else {
                        board.position(game.fen());
                    }
                });
            }
        } else {
            return 'snapback';
        }
    }

    function doMove(from, to, promo) {
        if(evaluation) {
            stopEvaluation();
        }

        var move = game.move({
            from: from,
            to: to,
            promotion: promo
        });

        // illegal move
        if (move === null) return 'snapback';

        gameHistory.push(move);
        currentPly++;

        if(promo) {
            board.position(game.fen());
        }

        highlightMove(null, null, move);

        findMove();
    }

    function onSnapEnd(from, to, piece) {
        board.position(game.fen());
    }

    function undo() {
        var currentColor = game.turn();
        stopEngine();
        stopEvaluation();

        e.result.text('');
        e.saveGameBtn.hide();

        if(game.undo()) {
            gameHistory.pop();
            if(currentPly % 2 == 0) {
                $('#game .move:last').remove();
            } else {
                $('#game .ply-' + currentPly).text('');
            }
            currentPly--;
        }

        if(currentColor != color) {
            if(game.undo()) {
                gameHistory.pop();
                if(currentPly % 2 == 0) {
                    $('#game .move:last').remove();
                } else {
                    $('#game .ply-' + currentPly).text('');
                }
                currentPly--;
            }
        }

        if((gameHistory.length - 1) >= 0) {
            toMove(gameHistory.length - 1);
        } else {
            game.reset();
            board.position('start');
        }

        if(color == game.turn()) {
            startEngine();
        }
        if(evaluation) {
            startEvaluation();
        }
    }

    function resign() {
        if(!confirm('Are you sure you want to resign?')) return;

        stopEngine();
        stopEvaluation();

        // show result
        var result = (color === 'w' ? '1-0' : '0-1');
        e.result.text(result);
        e.movesHolder.scrollTop(e.movesHolder[0].scrollHeight);

        e.status.text('Game over, ' + (color === 'w' ? 'Black' : 'White') + ' resigned');

        e.gameStatus.text((color === 'w' ? 'White' : 'Black') + ' Won!');

        e.saveGameBtn.show();
    }

    function highlightMove(oldPos, newPos, move) {
        if(!move) {
            $("#game [class^='ply-']").removeClass('highlight');
            e.board.find('.' + squareClass).removeClass('highlight');

            var move = gameHistory[currentPly];
            if(!move) { return; }

            e.board.find('.square-' + move.from).addClass('highlight');
            e.board.find('.square-' + move.to).addClass('highlight');

            var moveIndex = Math.ceil((currentPly + 1 + 1) / 2);
            if (move.color == 'w' && !$('#game .move-' + moveIndex).length) {
                var tr = '<tr class="move move-' + moveIndex + '"><td class="index">' + moveIndex + '</td><td class="ply-' + currentPly + '">' + move.san +
                    '</td><td class="eval-' + currentPly + '"></td><td class="ply-' + (currentPly + 1) + '"></td><td class="eval-' + (currentPly + 1) + '"></td></tr>';
                e.moves.append(tr);
            } else {
                e.moves.find('.ply-' + currentPly).text(move.san);
            }

            $("#game [class^='ply-']").removeClass('highlight');
            $('#game .ply-' + currentPly).addClass('highlight');

            e.movesHolder.scrollTop(e.movesHolder[0].scrollHeight);

            updateStatus();
        } else {
            e.board.find('.' + squareClass).removeClass('highlight');

            e.board.find('.square-' + move.from).addClass('highlight');
            e.board.find('.square-' + move.to).addClass('highlight');
        }
    }

    function updateStatus() {
        var status = '';

        var moveColor = 'White';
        if (game.turn() === 'b') {
            moveColor = 'Black';
        }

        if (game.in_checkmate() === true) { // checkmate?
            status = 'Game over, ' + moveColor + ' is in checkmate.';

            // show result
            var result = game.turn() === 'w' ? '0-1' : '1-0';
            e.result.text(result);
            e.movesHolder.scrollTop(e.movesHolder[0].scrollHeight);

            e.gameStatus.text((game.turn() === 'b' ? 'White' : 'Black') +' Won!');

            e.saveGameBtn.show();
        } else if (game.in_draw() === true) { // draw?
            status = 'Game over, drawn position';

            // show result
            e.result.text('1/2 - 1/2');
            e.movesHolder.scrollTop(e.movesHolder[0].scrollHeight);

            e.gameStatus.text('Game Drawn!');

            e.saveGameBtn.show();
        } else {  // game still on
            status = moveColor + ' to move';

            if (game.in_check() === true) {  // check?
                status += ', ' + moveColor + ' is in check';
            }

            if(game.turn() != color) {
                e.gameStatus.text('Your Move...');
            } else {
                e.gameStatus.text("I'm thinking...");
            }
        }

        e.status.text(status);
    }

    // -----

    function findMove() {
        var possibleMoves = game.moves();
        if (possibleMoves.length === 0) return;
        startEngine();
    }

    function startEngine() {
        if(engine) {
            stopEngine();
        }

        engine = new Worker('/stockfish/src/stockfish.js');
        engine.onmessage = parseAnalysis;

        engine.postMessage('ucinewgame');
        engine.postMessage('position fen ' + game.fen());
        engine.postMessage('setoption name Skill Level value ' + skillLevel);
        //engine.postMessage('setoption name Skill Level Maximum Error value ' + maxErr);
        //engine.postMessage('setoption name Skill Level Probability value ' + errProb);
        engine.postMessage('go depth ' + skillLevel);
    }

    function stopEngine() {
        if(engine) {
            engine.postMessage('quit');
            engine.terminate();
            engine = null;
        }
    }

    function parseAnalysis(ev) {
        var line = ev.data ? ev.data : ev;
        if(line.indexOf('bestmove') == -1) {
            return;
        }

        var parts = line.split(' ');
        var bestMove = parts[1];

        makeMove(bestMove);
    }

    function makeMove(bestMove) {
        var m = bestMove.match(/.{1,2}/g);

        var move = game.move({
            from: m[0],
            to: m[1],
            promotion: m[2] // h7h8q
        });

        gameHistory.push(move);
        currentPly++;

        board.position(game.fen());

        if(evaluation) {
            startEvaluation();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    function toggleEvaluation() {
        evaluation = !evaluation;

        e.evalOn.html('&nbsp;');
        e.evalBlackScale.css('height', '50%');

        if(evaluation) {
            e.evalBtn.text('Analysis Off');
            e.evalOff.hide();
            e.evalOn.show();

            startEvaluation();
        } else {
            e.evalBtn.text('Analysis On');
            e.evalOn.hide();
            e.evalOff.show();

            stopEvaluation();
        }
    }

    function startEvaluation() {
        if(evalEngine) {
            stopEvaluation();
        }
        e.evalScale.show();

        evalEngine = new Worker('/stockfish/src/stockfish.js');
        evalEngine.onmessage = parseEvaluation;

        evalEngine.postMessage('position fen ' + game.fen());
        evalEngine.postMessage('go depth ' + analysisDepth);
    }

    function stopEvaluation() {
        if(evalEngine) {
            evalEngine.postMessage('quit');
            evalEngine.terminate();
            evalEngine = null;

            e.evalOn.html('&nbsp;');
            e.evalBlackScale.css('height', '50%');
            e.evalScale.hide();
            e.board.find('.' + squareClass).removeClass('bestmove');
        }
    }

    function parseEvaluation(ev) {
        var line = ev.data ? ev.data : ev;
        if(line.indexOf('multipv') == -1) {
            return;
        }

        var info = new EngineMessage(line);
        var evalPercent;
        if(('' + info.evaluation).indexOf('#') == -1) {
            evalPercent = (50 - info.evaluation * 12.5).toFixed(0);
        } else {
            evalPercent = info.evaluation.replace('#', '')*1 > 0 ? 0 : 100;
        }

        if(scaleTimeout) { clearTimeout(scaleTimeout); }
        scaleTimeout = setTimeout(function() {
            e.evalBlackScale.animate({height: (evalPercent < 100 ? evalPercent + '%' : '100%')}, 200);
        }, 100);

        var evalTxt = info.evaluation > 0 ? '+' + info.evaluation : info.evaluation;
        e.evalOn.text(evalTxt);

        showBestMove(info.bestMove);
    }

    function showBestMove(move) {
        move = move.match(/.{1,2}/g);

        e.board.find('.' + squareClass).removeClass('bestmove');

        e.board.find('.square-' + move[0]).addClass('bestmove');
        e.board.find('.square-' + move[1]).addClass('bestmove');
    }

    function EngineMessage(line) {
        var _info = {};

        _info.message = line;
        _info.message = _info.message.trim();
        _info.message = _info.message.replace(/\s+/g,' ');

        _info.tokens  = _info.message.split(' ');

        // -----------------------------------------------------------------------------------------------------------------

        function getBestMove() {
            return getValue('pv', undefined);
        }

        function getEvaluation() {
            var normalize = game.turn() == 'b' ? -1 : 1;

            var mate = getValue('score', '') == 'mate';
            if(mate) {
                var score = getValue2('score', 0) * 1 * normalize;

                if(score > 0) {
                    return '#' + score;
                } else {
                    return '-#' + Math.abs(score);
                }
            } else {
                return ((getValue2('score', 0)/100).toFixed(2) * 1 * normalize).toFixed(2);
            }
        }

        // -----------------------------------------------------------------------------------------------------------------

        // first from key value
        function getValue(key, def) {
            var i = _info.tokens.indexOf(key);
            if(i != -1) {
                return _info.tokens[i+1];
            }
            return def;
        }

        // second from key value
        function getValue2(key, def) {
            var i = _info.tokens.indexOf(key);
            if(i != -1) {
                return _info.tokens[i+2];
            }
            return def;
        }

        // first from key to end
        function getValue3(key, def) {
            var i = _info.tokens.indexOf(key);
            if(i != -1) {
                return _info.tokens.slice(i+1).join(' ');
            }
            return def;
        }

        // -----------------------------------------------------------------------------------------------------------------

        return {
            bestMove: getBestMove(),
            evaluation: getEvaluation()
        }
    }


    function getPgn() {
        var pgn = [];
        $('#game tr').each(function(i, tr) {
            var $tr = $(tr);

            var tds = $tr.find('td');
            pgn.push($(tds[0]).text() + '. ' + $(tds[1]).text());
            if(tds.length > 2) {
                pgn.push($(tds[3]).text());
            }
        });

        //console.log(pgn.join(' ') + ' ' + $('#game .result').text());

        return pgn.join(' ') + ' ' + $('#game .result').text();
    }
};