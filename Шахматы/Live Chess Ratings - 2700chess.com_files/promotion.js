var Promotion = function() {
    var promotion = null;

    init();

    return {
        ask: ask,
        isNeeded: isNeeded
    };

    ////////////

    function init() {
        $('#promotionModal .variants a').on('click', setPromotion);
    }

    function setPromotion() {
        promotion = $(this).data('value');
    }

    function ask() {
        promotion = null;
        var modal = $('#promotionModal').modal({backdrop: 'static', keyboard: false});

        var def = $.Deferred();
        modal.on('hidden.bs.modal', function () {
            def.resolve(promotion);
        });

        return def.promise();
    }

    function isNeeded(from, to, piece, color) {
        // BUG: color is always 'white'
        if(piece != 'wP' && piece != 'bP') { // is pawn
            return false;
        } else if(piece[0] == 'w' && from[1] == 7  && to[1] == 8) {
            return true;
        } else if(piece[0] == 'b' && from[1] == 2  && to[1] == 1) {
            return true;
        } else {
            return false;
        }
    }
};